var teufels_thm_jq_transit__interval = setInterval(function () {

    if (typeof jQuery == 'undefined') {
    }  else {

        clearInterval(teufels_thm_jq_transit__interval);

        loadScript("/typo3conf/ext/teufels_thm_jq_transit/Resources/Public/Assets/Js/transit.min.js.gzip?v=1", function () {
            if (teufels_cfg_typoscript_sStage == "prototype" || teufels_cfg_typoscript_sStage == "development") {
                console.info('jQuery transit loaded');
            }
        });
    }

}, 2000);

