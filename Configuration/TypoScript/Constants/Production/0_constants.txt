plugin.tx_teufels_thm_jq_transit {
    settings {
        production {
            includePath {
                public = EXT:teufels_thm_jq_transit/Resources/Public/
                private = EXT:teufels_thm_jq_transit/Resources/Private/
                frontend {
                    public = typo3conf/ext/teufels_thm_jq_transit/Resources/Public/
                }
            }
            optional {
            	active = 0
            }
        }
    }
}